package com.ensof.util;

import com.ensof.mccube.McCubeSupport;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

////////////////////////////////////////////////////////////////////////////////
//Date Created : 2020-06-22 16:10:06
//Organization : enSOF Technology Inc.
//Author       : sunghere@ensof.co.kr
////////////////////////////////////////////////////////////////////////////////
//Revision History
//who          when         what


class HttpUtilTest {

    @Test
    void get() {
        String response = HttpUtil.get(McCubeSupport.PROJECT_RULE_URL);
//        System.out.println(response);
        assertThat(response).isNotNull();
        assertThat(response).startsWith("[ServerInfo]");
    }
}