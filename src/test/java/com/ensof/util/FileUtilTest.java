package com.ensof.util;

import org.junit.jupiter.api.Test;

import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;


////////////////////////////////////////////////////////////////////////////////
//Date Created : 2020-06-19 14:06:30
//Organization : enSOF Technology Inc.
//Author       : sunghere@ensof.co.kr
////////////////////////////////////////////////////////////////////////////////
//Revision History
//who          when         what


class FileUtilTest {


    @Test
    void moveFile() {
        String src = "D:\\ensof\\12.TEST\\moveFile";
        String dest = "D:\\ensof\\12.TEST\\moveFileAfter";
        FileUtil.copy(src, dest);
        assertThat(Paths.get(src)).exists();
        assertThat(Paths.get(dest)).exists();

    }
}