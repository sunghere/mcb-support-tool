package com.ensof.mccube;
////////////////////////////////////////////////////////////////////////////////
//Date Created : 2020-05-18 18:15:16
//Organization : enSOF Technology Inc.
//Author       : sunghere@ensof.co.kr
////////////////////////////////////////////////////////////////////////////////
//Revision History
//who          when         what


import com.ensof.docker.DockerConfig;
import com.ensof.util.*;

import java.io.*;
import java.util.*;

public class McCubeSupport {

    public final static String PROJECT_RULE_URL = "http://lab.ensof.co.kr/down.do?filename=dockerRule.ini";
    public static final String DEFALT_WORKBENCH_NAME = "workbench.zip";
    private static final String PORT_INFO = "ServerInfo";
    private static final HashMap EMPTY_MAP = new HashMap<>();
    //    private static final String MASKING_IP = "#.#.#.#";
    private static final String MASKING_IP = "localhost";



    public enum KEYS {
        host, index
    }

    public static String version = DockerConfig.MCCUBE_VERSION_35;
    private static final List<String> DEFALT_SRC_LIST = Arrays.asList("NsfAgentProcJ.jar", "NsfJavaagent15J.jar", "NsfJavaagentServerJ.jar", "McCubeServer.ver", "McCubeInstance.ver");
    private static final List<String> EXP_FILES = Arrays.asList("archive", "log", "userMemento", "handle", "backup", "tx", "event", "statistics", "transfer", "transferlog", "txlog");

    private static Map<String, Map<String, String>> ruleInfo;


    public static void setVersion(String version) {
        McCubeSupport.version = version;
    }

    public static Map<String, String> findEnvironment(String path) throws IOException {
        String startShellPath = path + "/bin/startServer";
        File serverVersionFile = new File(path + "/lib/McCubeServer.ver");

        File startShellFile = new File(startShellPath + ".sh");
        if (startShellFile.exists()) {
            Logger.println("startShellPath : " + startShellFile);
        } else if (new File(startShellPath + ".cmd").exists()) {
            startShellFile = new File(startShellPath + ".cmd");
            Logger.warn("정상 지원하지 않는 파일입니다.");
        } else {
            throw new FileNotFoundException();
        }

        Map<String, String> exportConfig = new HashMap<>();
        if (!startShellFile.exists()) throw new FileNotFoundException(startShellPath + " 파일을 찾을 수 없습니다.");
        String java;
        try {
            for (String line : FileUtil.read(startShellFile)) {
                if (isProductHomeLine(line)) {
                    exportConfig.put(DockerConfig.PRODUCT_HOME, getValueFromShell(line));
                } else if (isJavaLine(line)) {
                    java = getValueFromShell(line);
                    exportConfig.put(DockerConfig.JAVA_HOME, java);
                }
            }
            if (serverVersionFile.exists()) {
                for (String line : FileUtil.read(serverVersionFile)) {
                    if (line.startsWith("McCubeServerJ_")) {
                        exportConfig.put(DockerConfig.VERSION_MINOR, line.replace("McCubeServerJ_", "").replace(".jar", ""));
                    }
                }
            }
            exportConfig.put(DockerConfig.WORKBENCH_PORT, getAdminPortNum(path));
        } catch (Exception e) {
            Logger.print(e);
            throw e;
        }
        return exportConfig;
    }

    private static String getValueFromShell(String text) {
        String result = "";
        if (text == null) return result;
        String[] split = text.split("=");
        result = StringUtil.filePath(split[split.length - 1].trim().replaceAll("\"", ""));
        return result;
    }

    private static boolean isProductHomeLine(String text) {
        return text.contains(DockerConfig.PRODUCT_HOME + "=");
    }

    private static boolean isJavaLine(String text) {
        return text.contains(DockerConfig.JAVA_HOME + "=");
    }

    public static String getAdminPortNum(String mccubePath) throws IOException {
        String configfilePath = mccubePath + "/config/config.xml";
        String port = null;
        boolean mbeanFlag = false;
        for (String line : FileUtil.read(configfilePath)) {
            if (!mbeanFlag && line.contains("mbeanConnector")) {
                mbeanFlag = true;
            }
            if (mbeanFlag && line.contains("port")) {
                port = line.split("port=\"")[1].split("\"")[0];
                break;
            }
        }

        return port;
    }


    public static Set<String> getJarList(String libPath) {
        if (!libPath.endsWith("/")) libPath += "/";

        HashSet<String> libList = new HashSet<>();
        libList.add("NsfLauncherJ.jar");
        try {

            for (String fileName : DEFALT_SRC_LIST) {
                if (fileName.endsWith(".ver")) {
                    libList.addAll(FileUtil.read(libPath + fileName));
                } else {
                    Optional<String> classPathOp = Optional.ofNullable(FileUtil.jarReadGetManifest(libPath + fileName).getMainAttributes().getValue("Class-Path"));
                    classPathOp.ifPresent(s -> libList.addAll(Arrays.asList(s.split(" "))));
                }
            }
            libList.addAll(DEFALT_SRC_LIST);
        } catch (IOException e) {
            Logger.print(e);
        }
        return libList;
    }

    public static String export(Map<String, String> config) throws Exception {
        String mcCubeDir = config.get(DockerConfig.WORKDIR) + "/" + config.get(DockerConfig.SERVER_SRC_PATH);
        String lib = mcCubeDir + "/lib";
        ArrayList<String> expList = new ArrayList();
        expList.addAll(EXP_FILES);
        Set<String> jarList = McCubeSupport.getJarList(lib);
        if(!"true".equalsIgnoreCase(config.get(DockerConfig.TEST_INCLUDE))) {
            expList.add("test");
        }
        return FileUtil.makeZipFileWithFilter(mcCubeDir, expList, jarList);
    }

    public static String getProjectIndexByName(String key, String def) {
        String index = getProjectInfo().get(key);
        if (index == null) index = def;
        return index;
    }

    public static Map<String, String> getProjectInfo() {
        return getRuleInfo(PORT_INFO);
    }


    public static Map<String, String> getRuleInfo(String key) {
        if (ruleInfo == null) {
            ruleInfo = Property.parse(HttpUtil.get(PROJECT_RULE_URL));
        }
        Optional<Map<String, String>> ruleInfoOp = Optional.ofNullable(ruleInfo.get(key));
        return ruleInfoOp.orElse(EMPTY_MAP);
    }

    public static String getRuleInfo(String key, String subKey) {
        return Optional.ofNullable(getRuleInfo(key)).isPresent() ? getRuleInfo(key).get(subKey) : null;
    }

    public static void filterIP(InputStream in, OutputStream out) throws IOException {
        String regExp = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
        PrivacyIOUtil.copyWithReplacePrivacy(in, out, regExp, MASKING_IP, false, true);
    }

    public static void filterPrivacy(InputStream in, OutputStream out) throws IOException {
        PrivacyIOUtil.copyWithReplacePrivacy(in, out, null, null, false, true);
    }

    public static void filterJVM(InputStream in, OutputStream out) throws IOException {
        String regExp = "(-Xm[s|x])([1-9]{1}[0-9]*[m|M|g|G])";
        IOUtil.copyWithReplace(new BufferedReader(new InputStreamReader(in)), out, regExp, "$1" + "512m", true);
    }


  /*  public static void main(String[] args) {
        String regExp = "(-Xm[s|x])([1-9]{1}[0-9]*[m|M|g|G])";
        String str = "-Xms1024m -Xmx1G -Xmd1024G ";

        System.out.println(str.replaceAll(regExp, "$1"+"512m"));
    }*/


}
