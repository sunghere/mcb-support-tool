package com.ensof.mccube;
////////////////////////////////////////////////////////////////////////////////
//Date Created : 2020-06-22 16:03:13
//Organization : enSOF Technology Inc.
//Author       : sunghere@ensof.co.kr
////////////////////////////////////////////////////////////////////////////////
//Revision History
//who          when         what


import com.ensof.util.StringUtil;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Property {

    public static Map<String, Map<String, String>> parse(String data) {
        Map<String, Map<String, String>> property = new ConcurrentHashMap<>();
        String key = null;
        for (String s : data.split("\n")) {
            if (StringUtil.isEmpty(s)) {
                continue;
            } else if (s.startsWith("[")) {
                key = takeOff(s);
                property.put(key, new ConcurrentHashMap<>());
            } else {
                String[] tokens = s.split("=");
                if (StringUtil.notEmpty(key))
                    property.get(key).put(tokens[0], tokens[1]);
            }
        }

        return property;
    }

    private static String takeOff(String str) {
        return str.replaceAll("[\\[\\]]", "");

    }

}
