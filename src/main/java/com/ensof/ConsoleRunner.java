package com.ensof;
////////////////////////////////////////////////////////////////////////////////
//Date Created : 2020-05-14 11:11:13
//Organization : enSOF Technology Inc.
//Author       : sunghere@ensof.co.kr
////////////////////////////////////////////////////////////////////////////////
//Revision History
//who          when         what


import com.ensof.docker.DockerConfig;
import com.ensof.docker.DockerFileMaker;
import com.ensof.docker.DockerUtil;
import com.ensof.mccube.McCubeSupport;
import com.ensof.save.ConfigManger;
import com.ensof.util.DateUtil;
import com.ensof.util.FileUtil;
import com.ensof.util.Logger;
import com.ensof.util.StringUtil;
import org.beryx.textio.TextIO;
import org.beryx.textio.TextIoFactory;
import org.beryx.textio.TextTerminal;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

import static com.ensof.mccube.McCubeSupport.*;

public class ConsoleRunner {

    TextIO textIO;

    private static final String P_DOCKER = "Docker Image Create";
    private static final String P_MC_EXPORT = "Export";
    private static final String P_MC_DECODING = "Unzip (_e file)";

    public static void main(String[] args) {
        new ConsoleRunner().start();
    }

    public void init() {
        textIO = TextIoFactory.getTextIO();
        TextTerminal<?> terminal = textIO.getTextTerminal();
        Logger.setTextTerminal(terminal);
    }

    public void start() {
        init();
        ConfigManger.load();
        boolean work = true;
        boolean save = false;
        while (work) {
            String process = textIO.newStringInputReader()
                    .withNumberedPossibleValues(P_MC_EXPORT, P_MC_DECODING, P_DOCKER)
                    .read("Select Process");
            switch (process) {
                case P_DOCKER:
                    dockerProcess();
                    break;
                case P_MC_EXPORT:
                    exportProcess();
                    break;
                case P_MC_DECODING:
                    decodeProcess();
                    break;
                default:
                    work = false;
                    break;
            }
            ConfigManger.save();
        }

    }

    private String dockerBuild(DockerConfig config) {
        boolean build = textIO.newBooleanInputReader()
                .withDefaultValue(true)
                .withPropertiesPrefix("warn")
                .read("build?");
        if (!build) {
            return null;
        }
        String version = textIO.newStringInputReader()
                .withDefaultValue(config.get(DockerConfig.VERSION_MINOR) + ".0.1")
                .read("version");
        return DockerUtil.build(config, version);
    }

    private void decodeProcess() {
        String target = textIO.newStringInputReader()
                .read("Path (file or directory)");
        File targetFile = new File(target.trim());
        if (targetFile.isDirectory()) {
            boolean recursive = textIO.newBooleanInputReader()
                    .withDefaultValue(true)
                    .withPropertiesPrefix("warn")
                    .read(" Apply to all subdirectories?");
            decodeBase64(targetFile, recursive);
        } else {
            decodeBase64File(targetFile);
        }

    }

    private void decodeBase64(File targetDir, boolean recursive) {
        File[] flist = targetDir.listFiles();
        for (File f : flist) {
            if (f.isDirectory() && recursive) {
                decodeBase64(f, true);
            } else if (f.getName().endsWith("_e")) {
                decodeBase64File(f);
            }
        }
    }

    private void decodeBase64File(File f) {
        try {
            String resultFileName = f.getName().replace("_e", "") + ".zip";
            long lap = FileUtil.decodeBase64(f, resultFileName);
            Logger.println("decoded. path:" + f.getParentFile().getPath() + "/" + resultFileName + "[" + lap + "ms]");
        } catch (IOException e) {
            Logger.error(e);
        }
    }

    private void exportProcess() {
        Map<String, String> config = this.consoleReadMcCubeDirPath(null);
        config.put(DockerConfig.TEST_INCLUDE, this.consoleReadTestDataIsInclude() + StringUtil.EMPTY_STRING);

        try {
            long startTime = System.currentTimeMillis();
            Logger.println("export start. Time:" + DateUtil.getDateString("yyyyMMddHhmmss"));
            Logger.setMode(Logger.Type.File);
            String exportFile = McCubeSupport.export(config);
            Logger.setMode(Logger.Type.Screen);
            Logger.println("export end. Time:" + DateUtil.getDateString("yyyyMMddHhmmss") + " [" + (System.currentTimeMillis() - startTime) / 1000 + "s]");
            Logger.println("file: " + exportFile);
            boolean encode = textIO.newBooleanInputReader()
                    .withDefaultValue(true)
                    .withPropertiesPrefix("warn")
                    .read("compress?");
            if (encode) {
                FileUtil.encodeBase64(exportFile);
                boolean delete = new File(exportFile).delete();
                Logger.println(delete, exportFile + " delete done.");
            }
        } catch (Exception e) {
            Logger.print(e);
        } finally {
            Logger.setMode(Logger.Type.Screen);
        }

    }

    private void dockerProcess() {
        String version = textIO.newStringInputReader()
                .withNumberedPossibleValues(DockerConfig.MCCUBE_VERSION_35)
                .read("McCube Version");
        Logger.println(version + " version Docker image making start");
        try {
            McCubeSupport.setVersion(version);
            DockerConfig config = configSetting(version);

            long startTime = System.currentTimeMillis();
            Logger.println("DockerFile Create Start...");
            Logger.println("...");
            DockerFileMaker maker = new DockerFileMaker(config);
            maker.build();
            long lap = System.currentTimeMillis() - startTime;
            Logger.println("Complete! [" + lap + "ms]");

            //build
            String imageTag = dockerBuild(config);

            //push
            dockerPush(imageTag);

            //tagging
            dockerTag(imageTag);

        } catch (Exception e) {
            Logger.print(e);
        }

    }

    private void dockerPush(String imageTag) {
        if (imageTag == null) return;
        boolean push = textIO.newBooleanInputReader()
                .withDefaultValue(true)
                .withPropertiesPrefix("warn")
                .read(imageTag + " push?");
        if (!push) {
            return;
        }
        DockerUtil.push(imageTag);
    }


    private void dockerTag(String imageTag) {
        if (imageTag == null) return;
        boolean push = textIO.newBooleanInputReader()
                .withDefaultValue(false)
                .withPropertiesPrefix("warn")
                .read(imageTag + " tagging latest?");
        if (!push) {
            return;
        }
        String latestTag = DockerUtil.tagging(imageTag);
        DockerUtil.push(latestTag);
    }


    public DockerConfig configSetting(String version) {
        String text;

        DockerConfig config = new DockerConfig();
        config.set(DockerConfig.VERSION, version);


        // PROJECT
        text = textIO.newStringInputReader()
                .withNumberedPossibleValues(getProjectInfo().keySet().toArray(new String[getProjectInfo().size()]))
                .read("name(e.g. hkuk, kcb-card, nice, etc..):");
        config.set(DockerConfig.PROJECT, text);
        config.set(DockerConfig.INDEX, getProjectIndexByName(text, DockerConfig.DEFAULT_INDEX));
        config.set(DockerConfig.HOSTNAME, McCubeSupport.getRuleInfo(text, KEYS.host.name()));
//        Logger.println("project info [" + config + "]");
        // WORKDIR
        text = textIO.newStringInputReader()
                .read("working directory:");
        text = text.replaceAll("\\\\", "/");
        config.addAll(consoleReadMcCubeDirPath(text));


        // JAVA VERSION
        text = textIO.newIntInputReader()
                .withDefaultValue(8)
                .withMinVal(7)
                .withMaxVal(14)
                .read("Java Version(7-14)") + "";
        config.set(DockerConfig.JDK_VERSION, text);

        text = consoleReadWorkbenchPath(config.get(DockerConfig.WORKDIR));
        config.set(DockerConfig.WORKBENCH_SRC_PATH, text);
        return config;
    }


    public String consoleReadWorkbenchPath(String workDir) {
        boolean guard = true;
        String text = null;
        while (guard) {
            text = textIO.newStringInputReader()
                    .withDefaultValue(DEFALT_WORKBENCH_NAME)
                    .read("workbench path");
            String dockerBasedPath = workDir + "/" + text;
            if (!text.endsWith(".zip")) {
                Logger.println("supported only zip file.");
            } else if (new File(text).exists() && !text.contains(workDir)) {
                Logger.println("copy...[" + text + "->" + workDir + "/" + DEFALT_WORKBENCH_NAME + "]");
                dockerBasedPath = workDir + "/" + DEFALT_WORKBENCH_NAME;
                long copylap = FileUtil.copy(text, dockerBasedPath);
                text = DEFALT_WORKBENCH_NAME;
                Logger.println("copy complete. [" + copylap + "ms]");
            }

            if (new File(dockerBasedPath).exists()) {
                guard = false;
            } else {
                Logger.warn(new FileNotFoundException(text).getMessage());
            }
        }
        return "./" + text;
    }


    private boolean consoleReadTestDataIsInclude() {
        boolean include = textIO.newBooleanInputReader()
                .withDefaultValue(true)
                .withPropertiesPrefix("warn")
                .read("testCase include?");
        return include;
    }

    public Map<String, String> consoleReadMcCubeDirPath(String workDir) {
        boolean guard = true;
        String text;
        Map<String, String> env = null;
        while (guard) {
            try {
                text = textIO.newStringInputReader()
                        .withDefaultValue((workDir != null ? workDir : "/home/mccube") + "/McCubeServer")
                        .read("McCube Directory Path").replaceAll("\\\\", "/");
                String tempWokrDir = workDir;
                String mcConfigDirName = text.substring(text.lastIndexOf("/") + 1);
                if (workDir == null) {
                    tempWokrDir = text.substring(0, text.lastIndexOf("/"));
                } else if (!text.startsWith("./") && !text.substring(0, text.lastIndexOf("/")).equals(workDir)) {
                    Logger.println("copy...[" + text + "->" + workDir + "/" + mcConfigDirName + "]");
                    long lap = FileUtil.copy(text, workDir + "/" + mcConfigDirName);
                    Logger.println("copy complete.[" + lap + "ms]");
                }
                env = McCubeSupport.findEnvironment(tempWokrDir + "/" + mcConfigDirName);
                env.put(DockerConfig.WORKDIR, tempWokrDir.replaceAll("\\\\", "/"));
                env.put(DockerConfig.SERVER_SRC_PATH, "./" + mcConfigDirName);
                Logger.println("Verification completed!\n" +
                        "configuration [" + env + " ]");
                guard = false;
            } catch (Exception e) {
                Logger.print(e);
            }
        }
        return env;
    }
}
