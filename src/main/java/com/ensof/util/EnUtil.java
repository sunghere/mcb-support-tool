package com.ensof.util;
////////////////////////////////////////////////////////////////////////////////
//Date Created : 2020-06-18 16:17:18
//Organization : enSOF Technology Inc.
//Author       : sunghere@ensof.co.kr
////////////////////////////////////////////////////////////////////////////////
//Revision History
//who          when         what


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Objects;
import java.util.Optional;

import static java.lang.System.getProperty;

public class EnUtil {

    private static final int OS_CODE_WINDOWS = 0;
    private static final int OS_CODE_MAC = 1;
    private static final int OS_CODE_UNIX = 2;

    public static int getOSCode() {
        String os = getProperty("os.name").toLowerCase();
        if ((os.contains("win"))) {
            return OS_CODE_WINDOWS;
        } else if ((os.contains("mac"))) {
            return OS_CODE_MAC;
        } else if ((os.contains("hp-ux")) || (os.contains("aix")) || (os.contains("sunos")) || (os.contains("linux"))) {
            return OS_CODE_UNIX;
        }

        return -1;
    }

    @Deprecated
    public static String getJavaVersion(String javaHome) {
        String version = null;
        if (getOSCode() == OS_CODE_WINDOWS) {
            File[] paths = new File[]{new File(javaHome + "/jre/bin/server/jvm.dll"), new File(javaHome + "/bin/server/jvm.dll"),
                    new File(javaHome + "/jre/bin/client/jvm.dll"), new File(javaHome + "/bin/client/jvm.dll"),
                    new File(javaHome + "/jre/bin/j9vm/jvm.dll"), new File(javaHome + "/bin/j9vm/jvm.dll"),
                    new File(javaHome + "/jre/bin/classic/jvm.dll"), new File(javaHome + "/bin/classic/jvm.dll"),};

            File dllFile = null;
            for (File path : paths) {
                if (path.exists() && path.isFile()) {
                    dllFile = path;
                    break;
                }
            }
            if (dllFile == null) {
                Logger.warn("jvm.dll Not found");
            }
        }
        Optional<String[]> versions = Optional.ofNullable(getTargetJVMInfo(javaHome));
        if (versions.isPresent()) {
            String[] token = versions.get()[0].split("\\.");
            for (String s : token) {
                if (!s.equals("1")) {
                    version = s;
                    break;
                }
            }
        }
        return version;
    }

    private static String[] getTargetJVMInfo(String selectedJavaHome) {
        String[] tokens = null;
        Process process = null;
        InputStreamReader in = null;
        BufferedReader br = null;
        try {
            String resource = null;
            URL url = EnUtil.class.getResource("");
            if (url != null && url.getPath() != null) {
                resource = url.getPath().replaceAll("file:", "");
            }
            String classPath = Objects.requireNonNull(resource).split("!")[0];
            classPath = URLDecoder.decode(classPath, getProperty("file.encoding"));
            File dir = new File(new File(selectedJavaHome), "bin");
            String[] command = new String[]{
                    dir.getAbsolutePath() + File.separator + "java",
                    "-cp",
                    classPath,
                    ArchDataModelChecker.class.getName()
            };
            process = Runtime.getRuntime().exec(command, null);
            in = new InputStreamReader(process.getInputStream());
            br = new BufferedReader(in);
            String line = br.readLine();

            tokens = StringUtil.subString(line, '|');
            if (tokens.length == 2) {
                if ("null".equals(tokens[1])) {
                    tokens[1] = null;
                }
                Logger.println("java version :" + tokens[0] + (tokens[1] == null ? "" : tokens[1]) + "Bit");
            } else {
                StringBuffer commandBuf = new StringBuffer();
                for (String s : command) {
                    if (commandBuf.length() > 0) {
                        commandBuf.append(" ");
                    }
                    commandBuf.append(s);
                }
                Logger.println("java version check fail : " + commandBuf.toString());
            }
        } catch (Exception e) {
            Logger.error(e);
        } finally {
            if (process != null) {
                process.destroy();
            }
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ignored) {
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (Exception ignored) {
                }
            }
        }
        return tokens;
    }

    private static class ArchDataModelChecker {
        public static void main(String[] args) {
            System.out.println(getProperty("java.version") + "|" + getProperty("sun.arch.data.model"));
        }
    }
}
