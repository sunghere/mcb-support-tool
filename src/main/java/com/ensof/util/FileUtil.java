package com.ensof.util;
////////////////////////////////////////////////////////////////////////////////
//Date Created : 2020-05-13 16:09:11
//Organization : enSOF Technology Inc.
//Author       : sunghere@ensof.co.kr
////////////////////////////////////////////////////////////////////////////////
//Revision History
//who          when         what


import com.ensof.mccube.McCubeSupport;
import org.apache.commons.codec.binary.Base64OutputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.compress.utils.IOUtils;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.jar.JarInputStream;
import java.util.jar.Manifest;

import static java.nio.file.Files.createDirectories;

public class FileUtil {
    private static final List<String> EXP_FILES_EXT = Arrays.asList(".zip", ".tar",".war",".7z",".log",".back",".bak");
    private static int currentCount;
    private static int totalCount;
    private static int persent= -1;

    public static void write(String filePath, String data) throws IOException {
        write(filePath, data.getBytes(), false);
    }

    public static void write(String filePath, byte[] data, boolean append) throws IOException {
        File out = new File(filePath);

        if (out.exists() && !isOverwrite()) {
            throw new FileAlreadyExistsException(filePath);
        }

        FileOutputStream os = null;
        BufferedOutputStream bos = null;
        try {
            os = new FileOutputStream(filePath, append);
            bos = new BufferedOutputStream(os);
            bos.write(data);
        } finally {
            bos.close();
        }
    }

    public static List<String> read(String filepath) throws IOException {
        return read(new File(filepath));
    }

    public static List<String> read(File file) throws IOException {
        ArrayList<String> stringArray = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file));) {

            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                stringArray.add(line);
            }
        }
        return stringArray;
    }

    public static Manifest jarReadGetManifest(String filePath) {
        Manifest manifest = null;
        try (JarInputStream jarReader = new JarInputStream(new FileInputStream(filePath))) {
            manifest = jarReader.getManifest();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return manifest;
    }
    public static void libZip(ZipArchiveOutputStream archive, File srcDir, String pathHeader, Set<String> fileList) {
        archive.setEncoding("UTF-8");
        // Walk through files, folders & sub-folders.
        for (File file : srcDir.listFiles()) {

            if (!file.isDirectory() &&
                    (fileList != null && fileList.contains(file.getName()))) {
                Logger.println("Zipping file - " + file);
                ZipArchiveEntry entry_1 = new ZipArchiveEntry(file, file.toString().replaceAll("\\\\", "/").replace(pathHeader, ""));
                try (FileInputStream fis = new FileInputStream(file)) {
                    archive.putArchiveEntry(entry_1);
                    IOUtils.copy(fis, archive);
                    archive.closeArchiveEntry();
                } catch (IOException e) {
                    Logger.print(e);
                }
            }
        }
        // Complete archive entry addition.
        Logger.println("libary complete!");
    }

    public static String makeZipFileWithFilter(String srcDirPath, List<String> exp, Set<String> jarList) throws Exception {
        return makeZipFileWithFilter(srcDirPath, exp, jarList, "./McCubeExport." + new Date().getTime());
    }
    private static void totalCountAssemble(File srcDir,List<String> exp) throws IOException {
        if (srcDir.getName().equals("lib")) {
            return;
        } else if (exp.contains(srcDir.getName())) {
            return;
        }
        for (File file : srcDir.listFiles()) {
            String fileName = file.getName();
            if (exp.contains(fileName) || _nameFilter(fileName)) continue;
            if (!file.isDirectory()) {
                totalCount++;
            } else {
                totalCountAssemble(file, exp);
            }
        }
    }
    public static String makeZipFileWithFilter(String srcDirPath, List<String> exp, Set<String> jarList, String destPath) throws Exception {
        String pathHeader = srcDirPath.substring(0, srcDirPath.lastIndexOf("/") + 1);
        currentCount=0;
        totalCount =0;
        try (ZipArchiveOutputStream archive = new ZipArchiveOutputStream(new FileOutputStream(destPath))) {
            archive.setEncoding("UTF-8");
            File srcDir = new File(srcDirPath);

            //counting
            totalCountAssemble(srcDir, exp);
            Logger.println("total " + totalCount +" file detected.", Logger.Type.Screen);
            // Walk through files, folders & sub-folders.
            zipWithFilter(archive, srcDir, pathHeader, exp, jarList);
            // Complete archive entry addition.
            archive.finish();
        }
        return destPath;
    }

    private static void zipWithFilter(ZipArchiveOutputStream archive, File srcDir, String pathHeader, List<String> exp, Set<String> jarList) throws IOException {
        if (srcDir.getName().equals("lib")) {
            libZip(archive, srcDir, pathHeader, jarList);
            return;
        } else if (exp.contains(srcDir.getName())) {
            return;
        }
        for (File file : srcDir.listFiles()) {
            String fileName = file.getName();
            if (exp.contains(fileName) || _nameFilter(fileName)) continue;
            // Directory is not streamed, but its files are streamed into zip file with
            // folder in it's path
            if (!file.isDirectory()) {
                currentCount++;
                int progress = (int) (((double) currentCount /(double) totalCount) * 100);
                if(progress != persent) {
                    persent = progress;
                    Logger.println("["+progress+"%, "+currentCount+" / "+totalCount+"] Zipping file - " + file, Logger.Type.Screen);
                }
                Logger.println("["+currentCount+" / "+totalCount+"] Zipping file - " + file);
                ZipArchiveEntry entry_1 = new ZipArchiveEntry(file, file.toString().replaceAll("\\\\", "/").replace(pathHeader, ""));
                try (FileInputStream fis = new FileInputStream(file)) {
                    archive.putArchiveEntry(entry_1);
                    if (isFilterTarget(file.getName())) {
                        McCubeSupport.filterIP(fis, archive);
                        Logger.println("IP filtered- " + file.getName());
                    } else if (isJvmOptionTarget(file.getName())) {
                        McCubeSupport.filterJVM(fis, archive);
                        Logger.println("JVM filtered- " + file.getName());
                    } else {
                        if (fileName.endsWith(".jar")) {
                            IOUtils.copy(fis,archive);
                        } else {
                            McCubeSupport.filterPrivacy(fis, archive);
                        }
                    }
                    archive.closeArchiveEntry();
                }
            } else {
                // case directory
                zipWithFilter(archive, file, pathHeader, exp, jarList);
            }
        }
    }

    private static boolean _nameFilter(String fileName) {
        for (String filter : EXP_FILES_EXT) {
            if(fileName.contains(filter)) return true;
        }
        return false;
    }

    private static boolean isFilterTarget(String name) {
        return name != null && name.endsWith(".ini");
    }

    private static boolean isJvmOptionTarget(String name) {
        return name != null && name.endsWith(".sh");
    }

    public static boolean isOverwrite() {
        return true;
    }

    public static void encodeBase64(String exportFile) throws IOException {
        OutputStream bos = null;
        String resultFile = exportFile + "_e";
        try (FileInputStream fileInputStream = new FileInputStream(exportFile)) {
            bos = new Base64OutputStream(new FileOutputStream(resultFile), true);
            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = fileInputStream.read(buffer, 0, 8192)) != -1) {
                bos.write(buffer, 0, bytesRead);
            }
            Logger.println("complete. (file)" + resultFile);
        } finally {
            bos.close();
        }
    }

    public static long decodeBase64(File encodedFile,String resultFileName) throws IOException {
        long startTime = System.currentTimeMillis();
        OutputStream bos = null;
        try (FileInputStream fileInputStream = new FileInputStream(encodedFile)) {
            bos = new Base64OutputStream(new FileOutputStream(new File(encodedFile.getParentFile(),resultFileName)), false);
            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = fileInputStream.read(buffer, 0, 8192)) != -1) {
                bos.write(buffer, 0, bytesRead);
            }
        } finally {
            if (bos != null)
                bos.close();
        }
        return System.currentTimeMillis() - startTime;
    }

    public static long copy(String src, String dest) {
        long copyStartTime = System.currentTimeMillis();
        try {
            Path s = Paths.get(src);
            Path d = Paths.get(dest);
            if (new File(src).isDirectory()) {
                copyFolder(s, d);
            } else {
                copyFile(s, d, null);
            }
        } catch (Exception e) {
        }
        return System.currentTimeMillis()-copyStartTime;
    }

    public static void copyFolder(Path source, Path target, CopyOption... options)
            throws IOException {
        Files.walkFileTree(source, new SimpleFileVisitor<Path>() {

            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                    throws IOException {
                createDirectories(target.resolve(source.relativize(dir)));
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
                    throws IOException {
                copyFile(file, target.resolve(source.relativize(file)), options);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    public static void copyFile(Path src, Path dest, CopyOption... options) throws IOException {
        if (options == null) options = new StandardCopyOption[]{StandardCopyOption.REPLACE_EXISTING};
        Files.copy(src, dest, options);
    }


}
