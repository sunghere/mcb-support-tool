package com.ensof.util;
////////////////////////////////////////////////////////////////////////////////
//Date Created : 2020-12-23 14:32:35
//Organization : enSOF Technology Inc.
//Author       : sunghere@ensof.co.kr
////////////////////////////////////////////////////////////////////////////////
//Revision History
//who          when         what


import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PrivacyIOUtil {
    private static final LinkedHashMap<String, Pattern> REGEXP_PRIVACYS;
    static {
        REGEXP_PRIVACYS = new LinkedHashMap<>();
        REGEXP_PRIVACYS.put("주민등록번호", Pattern.compile("\\d{2}(0[1-9]|1[0-2])(0[1-9]|[12][0-9]|[3][01])([\\s-:\\.]?)[1-4][0-9]{6}"));
//        REGEXP_PRIVACYS.put("여권1", Pattern.compile("([M|S|R|O|D|m|s|r|o|d][0-9]{8})"));
//        REGEXP_PRIVACYS.put("여권2", Pattern.compile("([a-zA-Z]{2}[0-9]{7})"));
        REGEXP_PRIVACYS.put("면허", Pattern.compile("(\\d{2}[\\s-:\\.])(\\d{6}[\\s-:\\.])(\\d{2})"));
        REGEXP_PRIVACYS.put("휴대폰번호", Pattern.compile("(01[0|1|6|7|8|9][\\s-:\\.]?)(\\d{3,4}[\\s-:\\.]?)(\\d{4})"));
        REGEXP_PRIVACYS.put("신용카드", Pattern.compile("(\\d{4}[\\s-:\\.])(\\d{4}[\\s-:\\.])(\\d{4}[\\s-:\\.])(\\d{4})"));
//        REGEXP_PRIVACYS.put("건강보험", Pattern.compile("([1-9]\\d{10})"));
        REGEXP_PRIVACYS.put("계좌번호1", Pattern.compile("(\\d{3}[\\s-:\\.])(\\d{3}[\\s-:\\.])(\\d{6})"));
        REGEXP_PRIVACYS.put("계좌번호2", Pattern.compile("(\\d{4}[\\s-:\\.])(\\d{3}[\\s-:\\.])(\\d{6})"));
        REGEXP_PRIVACYS.put("계좌번호3", Pattern.compile("(\\d{6}[\\s-:\\.])(\\d{2}[\\s-:\\.])(\\d{6})"));
        REGEXP_PRIVACYS.put("계좌번호4", Pattern.compile("(\\d{6}[\\s-:\\.])(\\d{2}[\\s-:\\.])(\\d{6})"));
        REGEXP_PRIVACYS.put("계좌번호5", Pattern.compile("(\\d{3}[\\s-:\\.])(\\d{2}[\\s-:\\.])(\\d{5}[\\s-:\\.])(\\d{1})"));
        REGEXP_PRIVACYS.put("계좌번호6", Pattern.compile("(\\d{3}[\\s-:\\.])(\\d{6}[\\s-:\\.])(\\d{5})"));
        REGEXP_PRIVACYS.put("계좌번호7", Pattern.compile("(\\d{3}[\\s-:\\.])(\\d{6}[\\s-:\\.])(\\d{2}[\\s-:\\.])(\\d{3})"));
        REGEXP_PRIVACYS.put("외국인등록번호", Pattern.compile("(\\d{3}[\\s-:\\.])(\\d{6}[\\s-:\\.])(\\d{2}[\\s-:\\.])(\\d{3})"));
        REGEXP_PRIVACYS.put("이메일", Pattern.compile("([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})"));
        REGEXP_PRIVACYS.put("IPv6", Pattern.compile("(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]).){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]).){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))"));
    }
    public static String replacePrivacy(String str)  {
        for (Map.Entry<String, Pattern> entry : REGEXP_PRIVACYS.entrySet()) {
            Matcher matcher = entry.getValue().matcher(str);
            while (matcher.find()) {
                int sIndex = matcher.start();
                int eIndex = matcher.end();
                StringBuffer sb = new StringBuffer(str.substring(0, sIndex));
                while (!(sIndex++ == eIndex)) {
                    sb.append("*");
                }
                sb.append(str.substring(eIndex));
                Logger.println("privacy replaced " + entry.getKey() + " [(find:" + matcher.group() + ")(ori:" + str + ")]");
                str = sb.toString();
            }
        }
        return str;
    }
    public static void copyWithReplacePrivacy(InputStream in, OutputStream out, String regexp, String replace, boolean backup, boolean privacyFiltering) throws IOException {
        String line;
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        while (((line = reader.readLine()) != null)) {
            if (regexp != null) {
                String tempLine = line.replaceAll(regexp, replace);
                if (!privacyFiltering && backup && !tempLine.equals(line)) {
                    line = "#ori - " + line + "\n";
                    out.write(line.getBytes());
                }
                line = tempLine;
            }
            if (privacyFiltering) {
                line = replacePrivacy(line);
            }
            line += "\n";
            out.write(line.getBytes());
        }
    }
}
