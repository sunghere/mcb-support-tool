package com.ensof.util;
////////////////////////////////////////////////////////////////////////////////
//Date Created : 2020-05-20 09:24:21
//Organization : enSOF Technology Inc.
//Author       : sunghere@ensof.co.kr
////////////////////////////////////////////////////////////////////////////////
//Revision History
//who          when         what


import java.util.LinkedList;
import java.util.List;

public class StringUtil {
    public static final String EMPTY_STRING = "";

    public static String filePath(String path) {
        if (path == null) return null;
        return path.replaceAll("\\\\", "/");
    }

    public static String[] subString(String str, char delimiter) {
        String[] empty = new String[0];

        List<String> list = subStringToList(str, delimiter);
        if (list == null) {
            return empty;
        } else {
            return list.toArray(empty);
        }
    }

    private static List<String> subStringToList(String str, char delimiter) {
        if (str == null) {
            return null;
        }
        final char DUMMY = (char) -1;
        char[] chs = str.toCharArray();
        int sqIdx = -1;
        int dqIdx = -1;
        for (int i = 0; i < chs.length; i++) {
            if (chs[i] == '\'') {
                sqIdx = getSqIdx(delimiter, DUMMY, chs, sqIdx, i);
            } else if (chs[i] == '"') {
                dqIdx = getSqIdx(delimiter, DUMMY, chs, dqIdx, i);
            }
        }
        LinkedList<String> temp = new LinkedList<>();
        int s = 0;
        int i = 0;
        int c;
        while (i < chs.length) {
            if (chs[i] == delimiter) {
                c = i - s;
                String word = new String(chs, s, c).replace(DUMMY, delimiter);
                temp.add(word);
                i++;
                s = i;
            } else {
                i++;
            }
        }
        c = i - s;
        String word = new String(chs, s, c).replace(DUMMY, delimiter);
        temp.add(word);

        return temp;
    }

    private static int getSqIdx(char delimiter, char DUMMY, char[] chs, int sqIdx, int i) {
        if (sqIdx == -1) {
            sqIdx = i;
        } else {
            for (int j = sqIdx; j < i; j++) {
                chs[j] = (chs[j] == delimiter) ? DUMMY : chs[j];
            }
            sqIdx = -1;
        }
        return sqIdx;
    }

    public static boolean notEmpty(String s) {
        return !isEmpty(s);
    }

    public static boolean isEmpty(String s) {
        return (s == null || s.equals(""));
    }

}
