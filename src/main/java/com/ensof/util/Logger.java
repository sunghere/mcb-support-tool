package com.ensof.util;
////////////////////////////////////////////////////////////////////////////////
//Date Created : 2020-05-13 16:22:38
//Organization : enSOF Technology Inc.
//Author       : sunghere@ensof.co.kr
////////////////////////////////////////////////////////////////////////////////
//Revision History
//who          when         what


import org.beryx.textio.TextTerminal;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Logger {

    private static TextTerminal textTerminal;
    private static final String LOG_FILE_NAME_PREFIX = "log.";
    private static BufferedWriter log;
    public Logger(TextTerminal terminal) {
        textTerminal = terminal;
    }

    public static void setTextTerminal(TextTerminal textTerminal) {
        Logger.textTerminal = textTerminal;
    }

    private static Type mode = Type.Screen;

    public enum Type {
        Screen, File, All, Sys
    }
    public Logger() {
    }

    public static void setMode(Type mode) {
        Logger.mode = mode;

        if (log != null) {
            synchronized (log) {
                try {
                    log.flush();
                    log.close();
                } catch (IOException e) {
                } finally {
                    log = null;
                }
            }
        }
    }

    public static void println(String message) {
        print("> " + message + "\n");
    }
    public static void println(String message, Type mode) {
        print("> " + message + "\n", mode);
    }
    private static void print(String message, Type mode) {
        if (textTerminal == null || mode == Type.Sys) {
            System.out.print(message);
        } else {
            if(mode == Type.Screen || mode == Type.All) {
                textTerminal.print(message);
            }
            if(mode == Type.File || mode == Type.All) {
                try {
                    if (log == null) {
                        makeLogFile();
                    }
                    synchronized (log) {
                        log.write(message);
                    }
                } catch (IOException e) {
                }
            }

        }
    }

    private static void print(String message) {
        print(message, mode);
    }
    private static synchronized void makeLogFile() throws IOException {
        log = new BufferedWriter(new FileWriter(LOG_FILE_NAME_PREFIX +DateUtil.getYYMMddHHmm(), true));
    }

    public static void print(Exception e) {
        StringBuffer sb = new StringBuffer("\n[!!] " + e.toString() + "\n");
        for (StackTraceElement ele : e.getStackTrace()) {
            sb.append("\t" + ele.toString() + " \n");
        }
        print(sb.toString() + " \n");
    }

    public static void error(Exception e) {
        print(e);
    }

    public static void printShort(Exception e) {
        print("> " + e.getMessage() + " \n");
    }

    public static void warn(String message) {

        print("(!) " + message + "\n");

    }

    public static void println(boolean delete, String s) {
        if(delete) println(s);
    }
}
