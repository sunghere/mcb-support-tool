package com.ensof.util;
////////////////////////////////////////////////////////////////////////////////
//Date Created : 2020-06-22 16:03:29
//Organization : enSOF Technology Inc.
//Author       : sunghere@ensof.co.kr
////////////////////////////////////////////////////////////////////////////////
//Revision History
//who          when         what


import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

public class HttpUtil {

    public static String get(String targetURL) {

        return get(targetURL, false);
    }

    public static String get(String targetURL, boolean debug) {
        InputStream is = null;
        StringBuffer sb = new StringBuffer();
        try {
            URL url = new URL(targetURL);
            URLConnection urlConnection = url.openConnection();
            is = urlConnection.getInputStream();
            byte[] buffer = new byte[1024];
            int readBytes;
            while ((readBytes = is.read(buffer)) != -1) {
                sb.append(new String(buffer, 0, readBytes));
            }

        } catch (IOException e) {
            if (debug)
                Logger.error(e);
        }
        return sb.toString().replaceAll("\r", "");
    }
}
