package com.ensof.util;
////////////////////////////////////////////////////////////////////////////////
//Date Created : 2020-07-08 14:31:48
//Organization : enSOF Technology Inc.
//Author       : sunghere@ensof.co.kr
////////////////////////////////////////////////////////////////////////////////
//Revision History
//who          when         what


import java.io.*;
import java.util.Base64;

public class IOUtil {
    private static final int COPY_BUF_SIZE = 8024;

    private IOUtil() {
    }

    public static long copy(InputStream input, OutputStream output) throws IOException {
        return copy(input, output, COPY_BUF_SIZE);
    }

    public static long copy(InputStream input, OutputStream output, int buffersize) throws IOException {
        if (buffersize < 1) {
            throw new IllegalArgumentException("buffersize must be bigger than 0");
        } else {
            byte[] buffer = new byte[buffersize];

            long count;
            int n;
            for (count = 0L; -1 != (n = input.read(buffer)); count += (long) n) {
                output.write(buffer, 0, n);
            }

            return count;
        }
    }

    public static void copy(BufferedReader in, OutputStream out) throws IOException {
        copyWithReplace(in, out, null, null);
    }

    public static void copyWithReplace(InputStream in, OutputStream out, String regex, String replace) throws IOException {
        copyWithReplace(new BufferedReader(new InputStreamReader(in)), out, regex, replace, false);
    }

    public static void copyWithReplace(BufferedReader in, OutputStream out, String regexp, String replace, boolean backup) throws IOException {
        String line;
        while (((line = in.readLine()) != null)) {
            if (regexp != null) {

                String tempLine = line.replaceAll(regexp, replace);
                if (backup && !tempLine.equals(line)) {
                    line = "#ori - " + line + "\n";
                    out.write(line.getBytes());
                }
                line = tempLine;

            }
            line += "\n";
            out.write(line.getBytes());
        }
    }



    public static void copyWithReplace(BufferedReader in, OutputStream out, String regexp, String replace) throws IOException {
        copyWithReplace(in, out, regexp, replace, false);
    }

    public static void exec(String command) {
        BufferedReader bufferedReader = null;
        try {
            Process process = Runtime.getRuntime().exec(command);
            bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                Logger.println(line);
            }
            int exitVal = process.waitFor();
            Logger.println("Process" + (exitVal == 0 ? "Success" : "Fail"));
        } catch (IOException | InterruptedException e) {
            Logger.error(e);
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ignored) {
                }
            }
        }
    }

    /**
     * Write the object to a Base64 string.
     */
    public static String objToString(Serializable o) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(o);
        oos.close();
        return Base64.getEncoder().encodeToString(baos.toByteArray());
    }

    /**
     * Read the object from Base64 string.
     */
    public static Object stringToObj(String s) throws IOException,
            ClassNotFoundException {
        byte[] data = Base64.getDecoder().decode(s);
        ObjectInputStream ois = new ObjectInputStream(
                new ByteArrayInputStream(data));
        Object o = ois.readObject();
        ois.close();
        return o;
    }
}
