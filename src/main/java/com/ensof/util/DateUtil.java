package com.ensof.util;
////////////////////////////////////////////////////////////////////////////////
//Date Created : 2020-12-21 13:59:16
//Organization : enSOF Technology Inc.
//Author       : sunghere@ensof.co.kr
////////////////////////////////////////////////////////////////////////////////
//Revision History
//who          when         what


import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    public static final String yyyy_MM_dd_HH_mm="yyyyMMddHHmm";
    private static Date today = new Date();
    public static String getDateString(String format) {
        return new SimpleDateFormat(format).format(today);
    }
    public static String getYYMMddHHmm() {
        return getDateString(yyyy_MM_dd_HH_mm);
    }
}
