package com.ensof.docker;
////////////////////////////////////////////////////////////////////////////////
//Date Created : 2020-05-13 16:29:26
//Organization : enSOF Technology Inc.
//Author       : sunghere@ensof.co.kr
////////////////////////////////////////////////////////////////////////////////
//Revision History
//who          when         what


import com.ensof.util.StringUtil;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DockerConfig implements Serializable {


    public final static String DEFAULT_REGISTRY_URL = "ensof.co.kr:5000/";
    public static final String PROJECT = "PROJECT_NAME";
    public static final String DEFAULT_INDEX = "40";

    private ConcurrentHashMap<String, String> config = new ConcurrentHashMap<>();

    public final static String MCCUBE_VERSION_35 = "mccube35";
    public final static String MCCUBE_VERSION_2X = "mccube";
    public final static String SERVER_SRC_PATH = "SERVER_SRC";
    public final static String WORKBENCH_SRC_PATH = "WORKBENCH_SRC";
    public final static String WORKBENCH_DEST_PATH = "WORKBENCH_DEST";
    public final static String WORKBENCH_PORT = "WORKBENCH_PORT";
    public final static String VERSION = "MCCUBE_VERSION_MAJOR";
    public final static String VERSION_MINOR = "MCCUBE_VERSION_MINOR";
    public static final String WORKDIR = "WORKDIR";
    public static final String HOSTNAME = "HOSTNAME";
    public static final String INDEX = "INDEX";
    public static final String TEST_INCLUDE = "TEST_INCLUDE";


    public final static String REGISTRY_URL = "url";

    public final static String JDK_VERSION = "jdk";
    public final static String JAVA_HOME = "JAVA_HOME";
    public final static String PRODUCT_HOME = "PRODUCT_HOME";

    private final static String DEFAULT_SERVER_SRC_PATH = "./McCubeServer";
    private final static String DEFAULT_WORKBENCH_SRC_PATH = "./workbench.zip";
    private final static String DEFAULT_WORKBENCH_DEST_PATH = "/home/mccube/workbench.zip";
    private final static String DEFAULT_WORKBENCH_PORT = "3543";
    private final static String DEFAULT_WORKDIR = ".";
    private final static String DEFAULT_JDK_VERSION = "7";
    private final static String DEFAULT_VERSION = MCCUBE_VERSION_35;

    public DockerConfig() {
        set(REGISTRY_URL, DEFAULT_REGISTRY_URL);
        set(SERVER_SRC_PATH, DEFAULT_SERVER_SRC_PATH);
        set(WORKBENCH_SRC_PATH, DEFAULT_WORKBENCH_SRC_PATH);
        set(WORKBENCH_DEST_PATH, DEFAULT_WORKBENCH_DEST_PATH);
        set(WORKBENCH_PORT, DEFAULT_WORKBENCH_PORT);
        set(WORKDIR, DEFAULT_WORKDIR);
        set(VERSION, DEFAULT_VERSION);
        set(INDEX, DEFAULT_INDEX);
    }

    /**
     * @param key key
     * @param def default value
     * @return value or DefaultValue
     */
    public String get(String key, String def) {
        String result = config.get(key);
        return result != null ? result : def;
    }

    public String get(String key) {
        return config.get(key) == null ? "" : config.get(key);
    }

    public void set(String key, String value) {
        if (StringUtil.isEmpty(value)) return;
        config.put(key, value);
    }

    public void put(String key, String value) {
        this.set(key, value);
    }

    public boolean isLatestVersion() {
        return config.get(VERSION).equals(MCCUBE_VERSION_35);
    }

    @Override
    public String toString() {
        return getWorkDir();
    }

    private String getWorkDir() {
        return config != null ? config.get(DockerConfig.WORKDIR) : null;
    }

    public void addAll(Map<String, String> map) {
        config.putAll(map);
    }


}
