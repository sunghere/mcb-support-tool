package com.ensof.docker;
////////////////////////////////////////////////////////////////////////////////
//Date Created : 2020-05-11 10:14:39
//Organization : enSOF Technology Inc.
//Author       : sunghere@ensof.co.kr
////////////////////////////////////////////////////////////////////////////////
//Revision History
//who          when         what


import com.ensof.util.FileUtil;
import com.ensof.util.Logger;
import com.ensof.util.StringUtil;

import java.io.IOException;

public class DockerFileMaker {

    DockerConfig config;
    String home;

    public DockerFileMaker(DockerConfig config) {
        this.config = config;
        home = config.get(DockerConfig.PRODUCT_HOME);
    }

    public String makeStartShellContent() {
        Logger.println("makeStartShellContent() start");

        StringBuffer sb = new StringBuffer();
        sb.append("#!/bin/sh\n\n");
        sb.append("# ssh\n" +
                "service ssh start\n");
        if (config.isLatestVersion()) {
            sb.append("\n# mccube start (3.5)\n" +
                    "rm -rf " + home + "/bin/.pid\n" +
                    home + "/bin/startServer.sh\n");
        } else {
            sb.append("\n" +
                    "# mccube start (2.X)\n" +
                    "MCCUBE_HOME=" + home + "\n");
        }
        // loop
        sb.append("\nwhile true\n" +
                "do echo \"running...\"\n" +
                "sleep 10000\n" +
                "done");
        Logger.println("makeStartShellContent() end");
        return sb.toString();
    }

    public String makeDockerFileContent() {
        Logger.println("makeDockerFileContent() start");
        StringBuffer sb = new StringBuffer();

        sb.append("# 기본 이미지\n" +
                "# " + config.get(DockerConfig.REGISTRY_URL) + "base:{jdk version} 으로 기입합니다. \n" +
                "# supported jdk7, jdk8\n" +
                "# e.g. " + config.get(DockerConfig.REGISTRY_URL) + "base:jdk7\n\n");
        sb.append("FROM " + config.get(DockerConfig.REGISTRY_URL) + "base:" + DockerConfig.JDK_VERSION + config.get(DockerConfig.JDK_VERSION) + "\n\n");
        sb.append("# JAVA HOME Setting \n" +
                "RUN mkdir -p " + config.get(DockerConfig.JAVA_HOME) + "/bin\n" +
                "RUN ln -sf  /usr/bin/java " + config.get(DockerConfig.JAVA_HOME) + "/bin/java\n");

        sb.append("# 환경값 (변경 필요시 주석 해제 후 사용)\n" +
                "#ENV LANG=ko_KR.EUC_KR\n" +
                "#ENV LANGUAGE=ko_KR.ko\n" +
                "ENV LC_ALL=C.UTF-8\n\n");
        sb.append("# 작업대상 디렉토리 지정\n" +
                "# 로컬 작업시 개인 디렉토리\n" +
                "# Lab(portainer) 작업시 본인이 업로드한 McCube 구성 경로\n" +
                "# e.g. WORKDIR /home/mccube/_2.5/IBK_FUND/20200428\n\n");
        sb.append("ENV WORK=\"" + config.get(DockerConfig.WORKDIR) + "\"\n" +
                "WORKDIR ${WORK}\n\n");

        if (config.isLatestVersion()) {
            sb.append("### McCube Workbench Port(McCube 설치할때 기입한 포트)\n" +
                    "EXPOSE " + config.get(DockerConfig.WORKBENCH_PORT) + "\n\n");
            sb.append("##### McCube WorkBench (COPY [원본파일] [Dest], 원본 파일명에 맞게 수정 할 것, Dest는 수정금지)\n" +
                    "# COPY [로컬경로] [Container상 경로]\n" +
                    "COPY " + config.get(DockerConfig.WORKBENCH_SRC_PATH) + " /home/mccube/workbench.zip\n\n");
        }
        sb.append("# COPY [로컬경로] [Container상 McCube 설치 경로(bin/startServer.sh/PRODUCT_HOME 참고)]\n" +
                "COPY " + config.get(DockerConfig.SERVER_SRC_PATH) + " " + home + "\n\n");
        sb.append("# 실행 - 수정금지\n" +
                "COPY ./start.sh /home/mccube/start.sh\n" +
                "RUN chmod +x /home/mccube/start.sh\n" +
                "ENTRYPOINT [\"/home/mccube/start.sh\"]");
        Logger.println("makeDockerFileContent() end");
        return sb.toString();
    }

    public void build() throws IOException {
        FileUtil.write(this.config.get(DockerConfig.WORKDIR) + "/" + "start.sh", makeStartShellContent());
        FileUtil.write(this.config.get(DockerConfig.WORKDIR) + "/" + "Dockerfile", makeDockerFileContent());
        FileUtil.write(this.config.get(DockerConfig.WORKDIR) + "/" + "DockerBuild.cmd", makeBuildScriptContent());
        FileUtil.write(this.config.get(DockerConfig.WORKDIR) + "/" + "DockerRun.cmd", makeStartScriptContent());
        Logger.println("build complete.");
    }

    private String makeBuildScriptContent() {
        Logger.println("makeDockerFileContent()");
        return "SET version=\"0.1\"\n" +
                "\n" +
                "if NOT \"%1\" == \"\" SET version=%1\n" +
                "\n" +
                "echo %version%\n" +
                "docker build --tag " + getTag() + ".%version% .";
    }

    private String makeStartScriptContent() {
        Logger.println("makeStartScriptContent()");
        String index = config.get(DockerConfig.INDEX);
        StringBuffer sb = new StringBuffer("SET version=latest\n");
        sb.append("\n");
        sb.append("if NOT \"%1\" == \"\" SET version=%1\n");
        sb.append("docker run -itd -p " + index + "043:" + config.get(DockerConfig.WORKBENCH_PORT));
        sb.append(" -p " + index + "022:22 ");
        if (StringUtil.notEmpty(config.get(DockerConfig.HOSTNAME))) {
            sb.append("-h ");
            sb.append(config.get(DockerConfig.HOSTNAME) + " ");
        }
        sb.append("--name " + getName() + " " + getTag() + ".%version%");
        return sb.toString();
    }

    private String getTag() {
        return config.get(DockerConfig.REGISTRY_URL) + getName() + ":" + config.get(DockerConfig.VERSION_MINOR);
    }

    private String getName() {
        return config.get(DockerConfig.VERSION) + "-" + config.get(DockerConfig.PROJECT);
    }
}
