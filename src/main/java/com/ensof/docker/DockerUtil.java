package com.ensof.docker;
////////////////////////////////////////////////////////////////////////////////
//Date Created : 2020-05-20 13:34:13
//Organization : enSOF Technology Inc.
//Author       : sunghere@ensof.co.kr
////////////////////////////////////////////////////////////////////////////////
//Revision History
//who          when         what


import com.ensof.util.IOUtil;
import com.ensof.util.Logger;
import com.ensof.util.StringUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static com.ensof.docker.DockerConfig.DEFAULT_REGISTRY_URL;

public class DockerUtil {



    public static void push(String imageTag) {

        String command = "docker push " + imageTag;
        Logger.println("Docker Push Start...");
        IOUtil.exec(command);
    }

    public static String tagging(String imageTag) {
        String latestTag = StringUtil.subString(imageTag, ':')[0] + ":latest";
        String command = "docker tag " + imageTag + " " + latestTag;
        IOUtil.exec(command);
        return latestTag;
    }

    public static String build(DockerConfig config, String version) {
        String imageTag = null;
        String command = "docker build --tag " + DEFAULT_REGISTRY_URL
                + config.get(DockerConfig.VERSION)
                + "-" + config.get(DockerConfig.PROJECT)
                + ":" + version
                + " " + config.get(DockerConfig.WORKDIR);
        Logger.println("Docker Build Start...");
        BufferedReader bufferedReader = null;
        try {
            Process process = Runtime.getRuntime().exec(command);
            bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                Logger.println(line);
                if (line.startsWith("Successfully tagged ")) {
                    imageTag = line.replace("Successfully tagged ", "");
                }
            }
            int exitVal = process.waitFor();
            Logger.println("Process" + (exitVal == 0 ? "Success" : "Fail"));
        } catch (IOException | InterruptedException e) {
            Logger.error(e);
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ignored) {
                }
            }
        }

        return imageTag;
    }


}
