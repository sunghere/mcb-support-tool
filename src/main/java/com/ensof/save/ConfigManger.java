package com.ensof.save;
////////////////////////////////////////////////////////////////////////////////
//Date Created : 2020-09-16 16:51:04
//Organization : enSOF Technology Inc.
//Author       : sunghere@ensof.co.kr
////////////////////////////////////////////////////////////////////////////////
//Revision History
//who          when         what


import com.ensof.docker.DockerConfig;
import com.ensof.util.FileUtil;
import com.ensof.util.IOUtil;
import com.ensof.util.Logger;
import com.ensof.util.StringUtil;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class ConfigManger {

    private static final LinkedHashMap<String, DockerConfig> configList = new LinkedHashMap() {
        @Override
        public String toString() {
            StringBuffer sb = new StringBuffer();
            configList.forEach((k, v) -> {
                try {
                    sb.append(k + "^" + IOUtil.objToString(v) + "\n");
                } catch (IOException e) {
                    Logger.error(e);
                }
            });
            return sb.toString();
        }
    };

    {
        load();
    }

    public static void print() {

        Logger.println(configList.toString());

    }

    public static void save() {


        try {
            FileUtil.write("./.config", configList.toString());
        } catch (IOException e) {
            Logger.error(e);
        }
    }

    public static void load() {
        List<String> lines = null;
        try {
            lines = FileUtil.read("./.config");

            HashMap<String, DockerConfig> list = new HashMap<>();
            lines.forEach(s -> {
                String[] strings = StringUtil.subString(s, '^');
                if (strings.length == 2) {
                    try {
                        list.put(strings[0], (DockerConfig) IOUtil.stringToObj(strings[1]));
                    } catch (IOException e) {
                    } catch (ClassNotFoundException e) {
                        Logger.error(e);
                    }
                }
            });
        

        } catch (FileNotFoundException e) {
        } catch (IOException e) {
            Logger.error(e);
        }

    }


    private LinkedHashMap<String, DockerConfig> getConfigList() {
        return configList;
    }

    public static void add(DockerConfig config) {
        if (config != null)
            configList.put(config.toString(), config);
    }

}
